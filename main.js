'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const logger = require('morgan');
const spawn = require('child_process').spawn;
const fs = require('fs');
const path = require('path');

function dvar(variable, value) { return (variable) ? variable : value; }

function ShowByteMemValue(sz_) {
	let sz = parseInt(sz_);
	if(sz > 1024 * 1024 * 1024) sz = sz / 1024 / 1024 / 1024 + ' gb';
	else if(sz > 1024 * 1024) sz = sz / 1024 / 1024 + ' mb';
	else if(sz > 1024) sz = sz / 1024 + ' kb';
	else sz += ' bytes';
	return sz;
}

const config = {
	exe: '../WebControl/bin/Debug/WebControl.exe'
};

let test = '123123123';

const webControlProc = spawn(config.exe);

const WebControl = {
	screen(resultStream, opt, complete) {
		webControlProc.stdin.write('screen '+opt.screenIndex+' '+opt.quality+' '+opt.compression+' '+opt.width+' '+opt.height+';', 'utf8');

		var callback = function(data) {
			webControlProc.stdout.removeListener('data', callback);
			complete(data);
		};

		webControlProc.stdout.on('data', callback);
	},

	info(complete) {
		webControlProc.stdin.write('info;', 'utf8');

		var callback = function(data) {
			var json = JSON.parse(data.toString('utf8'));
			webControlProc.stdout.removeListener('data', callback);
			complete(json);
		};

		webControlProc.stdout.on('data', callback);
	},
	
	control(opt, complete) {
		webControlProc.stdin.write('control '+opt.isMouse+' '+opt.isKeyboard+' '+opt.key+' '+opt.mouseX+' '+opt.mouseY+' '+opt.mouseLeftButton+' '+opt.mouseRightButton+';', 'utf8');

		var callback = function(data) {
			webControlProc.stdout.removeListener('data', callback);
			complete(data);
		};

		webControlProc.stdout.on('data', callback);
	},

	drive(complete) {
		webControlProc.stdin.write('drive;', 'utf8');

		var callback = function(data) {
			var json = JSON.parse(data.toString('utf8'));
			webControlProc.stdout.removeListener('data', callback);
			complete(json);
		};

		webControlProc.stdout.on('data', callback);
	}
};

const app = express();
app.set('view engine', 'jade');
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
	WebControl.info((info) => {
		res.locals.info = info;
		WebControl.drive((drives) => {
			for(let i = 0; i < drives.length; ++i) {
				if(drives[i].IsReady == 'True') {
					drives[i].AvailableFreeSpace = ShowByteMemValue(drives[i].AvailableFreeSpace);
					drives[i].TotalFreeSpace = ShowByteMemValue(drives[i].TotalFreeSpace);
					drives[i].TotalSize = ShowByteMemValue(drives[i].TotalSize);
				}
			}
			res.locals.drives = drives;
			res.render('index');
		});
	});
});

app.get('/screen/:screenIndex', function (req, res) {
	WebControl.screen(res, {
		screenIndex: dvar(req.params.screenIndex, 0),
		quality: dvar(req.query.quality, 50),
		compression: dvar(req.query.compression, 50),
		width: dvar(req.query.width, -1),
		height: dvar(req.query.height, -1)
	}, function(data){ res.end(data); });
});

app.get('/screenControl/:screenIndex', function (req, res) {
	res.locals.screenIndex = dvar(req.params.screenIndex, 0);
	res.render('screenControl');
});

app.post('/control', function (req, res) {
	WebControl.control({
		isMouse: dvar(req.body.isMouse, 'False'),
		isKeyboard: dvar(req.body.isKeyboard, 'False'),
		key: dvar(req.body.key, '{HOME}'),
		mouseX: dvar(req.body.mouseX, 0),
		mouseY: dvar(req.body.mouseY, 0),
		mouseLeftButton: dvar(req.body.mouseLeftButton, 'False'),
		mouseRightButton: dvar(req.body.mouseRightButton, 'False')
	}, function(data){ res.end(data); });
});

app.get('/filesystem', function (req, res) {
	let currentPath = req.query.path ? req.query.path : 'C:/';
	fs.readdir(currentPath, function(err, files) {
		if(err) {
			console.log(err);
			res.end('Error: '+err);
		}
		else {
			for(let i = 0; i < files.length; ++i) {
				try {
					let stat = fs.lstatSync(path.join(currentPath, files[i]));
					
					let sz = ShowByteMemValue(stat.size);
					
					files[i] = {
						isDir: stat.isDirectory(),
						name: files[i],
						size:  stat.isDirectory() ? '' : sz
					};
				} catch(err) {
					files[i] = {
						isDir: false,
						name: files[i],
						size: ''
					};
				}
			}
			res.locals = {
				files: files,
				currentPath: currentPath,
				fs: fs,
				path: path
			};
			res.render('filesystem');
		}
	});
});

app.get('/download_file', (req, res) => {
	try {
		fs.readFile(req.query.path, (err, data) => {
			if(err) res.end(err.toString());
			res.end(data);
		});
	} catch(err) {
		res.end(err.toString());
	}
});

app.listen(3000, function () {
	console.log('Listening on port 3000!');
});
